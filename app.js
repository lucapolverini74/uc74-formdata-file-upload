(function (global) {
    'use strict';

    // OS
    const fs = require('fs');
    const path = require('path');
    // Server
    const express = require('express');
    const formData = require('express-form-data');
    const http = require('http');
    const bodyParser = require('body-parser');

    var app = express();

    
    // Middleware

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));

    const multipartyOptions = {
        autoFiles: true
      };
       
      // parse a data with connect-multiparty. 
      app.use(formData.parse(multipartyOptions));
      // clear all empty files (size == 0)
      app.use(formData.format());
      // change file objects to node stream.Readable 
      app.use(formData.stream());
      // union body and files
      app.use(formData.union())

    var logger = function (req, res, next) {
        console.log('new req', req.url);
        next();
    };

    app.use(logger);
    

    app.put('/upload', function (req, res, next) {
        console.log('BODY', req.body);
        console.log('FILES', req.files);
        res.status(200).send({});
    });

    app.post('/upload', function (req, res, next) {
        console.log('BODY', req.body);
        console.log('FILES', req.files);
        res.status(200).send({});
    });

    var HTTP_PORT = 3000;
    var httpServer = http.createServer(app);

    httpServer.listen(HTTP_PORT, function () {
        console.log("Server Started at http://localhost:" + HTTP_PORT + "/");
        console.warn('Server base directory: ' + __dirname);
    });
    httpServer.on('error', function (e) {
        console.log('ERROR:', e);
    });
})();
